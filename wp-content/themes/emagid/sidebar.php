<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside class="blog_sidebar" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/bg_sidebar.png)">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
    
    <ul class="featured_blogs">
        <h2>Featured Blogs</h2>
                                <?php
            $args = array(
      'post_type' => 'blog',
        'posts_per_page' => 2
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>
    
        <li>
    <h5><?php the_field('title'); ?></h5>
                        <p><?php the_field('short_intro'); ?></p>
            <a href="<?php the_permalink(); ?>"><h6>Read Now</h6></a>
                </li>
    <?php
            }
                  }
            else  {
            echo 'No Posts Found';
            }
      ?>
        
        </ul>
</aside><!-- #secondary -->
