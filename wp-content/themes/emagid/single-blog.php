<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>

	<div class="main_content">
        
        <div class="main_hero" id="single" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/hero.jpg)">
            <div class="main_hero_text">
                <h1><?php the_field('title'); ?></h1>
            </div>
        </div>
		<main class="site_main">
            
            <div class="site_title">
                <h2><span>By Troy Osinoff</span></h2>
                <h4>Written: <? wp_days_ago_v3(); ?></h4>
            </div>
            
            
            
            <div class="blog_posts">
        
            
                <div class="post" id="single_post">
                    <div class="post_content" >
                        <p><?php the_field('full_post'); ?></p>
                    </div>
                </div>
            
                </div>

		</main><!-- #main -->
                <?php
get_sidebar();?>

</div>

<?php
get_footer();
