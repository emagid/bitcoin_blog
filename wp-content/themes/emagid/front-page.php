<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>

	<div class="main_content">
        
        <div class="main_hero" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/hero.jpg)">
            <div class="main_hero_text">
                                            <?php
            $args = array(
      'post_type' => 'blog',
        'posts_per_page' => 1
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>
                <h1>Bitcoins Future</h1>
                <a href="<?php the_permalink(); ?>"><p>Read Now</p></a>
                                <?php
            }
                  }
            else  {
            echo 'No Posts Found';
            }
      ?>
            </div>
        </div>
		<main class="site_main">
            
            <div class="site_title">
                <h2><span>Featured Blogs</span></h2>
            </div>
            
            <div class="blog_posts">
            
                            <?php
            $args = array(
      'post_type' => 'blog'
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>
            
                <div class="post">
                    <div class="post_image" style="background-image:url(<?php the_field('image'); ?>)">
                        <h6><?php the_category(); ?></h6>
                    </div>
                    <div class="post_content">
                        <h2><?php the_field('title'); ?></h2>
                        <p><?php the_field('short_intro'); ?></p>
                        <h6><? wp_days_ago_v3(); ?></h6>
                        <a href="<?php the_permalink(); ?>"><h5>Read Now</h5></a>
                        
                    </div>
                </div>
            
            
                <?php
            }
                  }
            else  {
            echo 'No Posts Found';
            }
      ?>
                </div>

		</main><!-- #main -->
                <?php
get_sidebar();?>

</div>

<?php
get_footer();
